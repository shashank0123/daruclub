import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Crop } from '@ionic-native/crop/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

declare var Razorpay: any;
var RazorpayCheckout: any;


@Component({
  selector: 'app-bookingpage',
  templateUrl: './bookingpage.page.html',
  styleUrls: ['./bookingpage.page.scss'],
})
export class BookingpagePage implements OnInit {
  qty: number = 0;
  RazorpayCheckout: any;
  Razorpay: any;
  data = '';
  bar_id = '';
  url = 'https://daru.club/api/menu_items';
  billurl = 'https://daru.club/api/uploadbill?type='+this.data;
  results: any;

  paymentAmount: number = 0;
  currency: string = 'INR';
  currencyIcon: string = '$';
  razor_key = 'rzp_test_ZHJ9fpmprslYAR';
  cartDetails: any = [];
  baseUrl: string = this.url;
  public user: object = {};
  public user_details: object = {};
  public company_details: object = {};
  public requestData: object = {};
  public responseData: object = {};
  // razor_key = 'rzp_live_k0c0GLsQr9YGp8';
  razor_secret = 'VHBhOBkYZiEZ0iwsHGiOuKxO';
  // currency = 'INR';
  public razorMessage: any;
  redirectBackUrl: any;
  fileUrl: any = null;
  respData: any;


  constructor(private route: ActivatedRoute, private http: HttpClient, private storage: Storage, private toastController: ToastController,private imagePicker: ImagePicker,
  private crop: Crop,
  private transfer: FileTransfer) { }

  ngOnInit() {
    this.bar_id = this.route.snapshot.paramMap.get("bar_id")
    this.route.queryParams.subscribe(params => {
      this.bar_id = params.bar_id;
      this.getMenuItemData();
    })

  }

  //
  getMenuItemData() {
    this.http.get(`${this.url}` + '?bar_id=' + this.bar_id, this.results).subscribe((res) => {
      this.results = res['bars'];
      this.setDefaultQty();
      console.log(this.results)
    });
  }

  setDefaultQty() {
    this.results.forEach((item, index) => {
      Object.assign(item, { qty: 0 });
    });
  }


  // for  add product in cart

  increamentQty(data, index) {
    this.results[index]['qty']++;
    this.paymentAmount += this.results[index]['price'];
    // this.qty++;
  }

  decreamentQty(data, index) {
    // var qt = this.qty;
    if (this.results[index]['qty'] >= 1) {
      this.results[index]['qty']--;
      this.paymentAmount -= this.results[index]['price'];
    }
  }

   payWithRazor(paymentAmount: Number) {
    const amount = Number(paymentAmount) * 100;
    var options = {
      description: '',
      // callback_url: this.redirectBackUrl,
      // redirect: true,
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: this.currency, // your 3 letter currency code
      key: this.razor_key, // your Key Id from Razorpay dashboard
      amount: amount, // Payment amount in smallest denomiation e.g. cents for USD
      name: 'Daruclub',
      "handler": function(response) {
        console.log(response);
        // this.razorPayCallBack(response);
        if (response['razorpay_payment_id']) {
          this.razorMessage = 'Transaction successfull with ' + response['razorpay_payment_id'];
          // this.responseData['error_message'] = null;
          // alert('Payment is successfull admin will contact you soon');
          // let alert = this.alertCtrl.create({
          //   title: 'Transaction Successfull',
          //   subTitle: 'Transaction is successfull admin will contact you soon',
          //   buttons: ['Dismiss']
          // });
          // alert.present();
        } else {
          this.razorMessage = 'Transaction error with ' + response;
          alert('Something went wrong ');
        }
      },
      prefill: {
        email: 'rajbabuhome@gmail.com',
        contact: '9560755823',
        name: 'shashank'
      },
      theme: {
        color: '#339b47'
      },

    };


    var successCallback = function(payment_id) {
      // alert('payment_id: ' + payment_id);
      this.responseData['success_message'] = 'Transaction successfull with ' + payment_id;
      this.responseData['error_message'] = null;
    };

    var cancelCallback = function(error) {
      // alert(error.description + ' (Error ' + error.code + ')');
      this.responseData['error_message'] = 'Transaction error with ' + error.code;
      this.responseData['success_message'] = null;
    };
    var razorPay = new Razorpay(options, successCallback, cancelCallback);
    razorPay.open(successCallback, cancelCallback);
  }
  // razorPayCallBack
  razorPayCallBack(response) {
    if (response['razorpay_payment_id']) {
      this.responseData['success_message'] = 'Transaction successfull with ' + response['razorpay_payment_id'];
      this.responseData['error_message'] = null;
    } else {
      this.responseData['error_message'] = 'Transaction error with ' + response.code;
      this.responseData['success_message'] = null;
    }
  }

  cropUpload() {
  this.imagePicker.getPictures({ maximumImagesCount: 1, outputType: 0 }).then((results) => {
    for (let i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
        this.crop.crop(results[i], { quality: 100 })
          .then(
            newImage => {
              console.log('new image path is: ' + newImage);
              const fileTransfer: FileTransferObject = this.transfer.create();
              const uploadOpts: FileUploadOptions = {
                 fileKey: 'file',
                 fileName: newImage.substr(newImage.lastIndexOf('/') + 1)
              };

              fileTransfer.upload(newImage, this.billurl, uploadOpts)
               .then((data) => {
                 console.log(data);
                 this.respData = JSON.parse(data.response);
                 console.log(this.respData);
                 this.fileUrl = this.respData.fileUrl;
               }, (err) => {
                 console.log(err);
               });
            },
            error => console.error('Error cropping image', error)
          );
    }
  }, (err) => { console.log(err); });
}


}
