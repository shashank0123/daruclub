import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingpagePage } from './bookingpage.page';

describe('BookingpagePage', () => {
  let component: BookingpagePage;
  let fixture: ComponentFixture<BookingpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingpagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
