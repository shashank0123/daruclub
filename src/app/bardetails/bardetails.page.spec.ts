import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BardetailsPage } from './bardetails.page';

describe('BardetailsPage', () => {
  let component: BardetailsPage;
  let fixture: ComponentFixture<BardetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BardetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BardetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
