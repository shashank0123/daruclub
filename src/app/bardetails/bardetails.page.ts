import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
declare var google;

@Component({
  selector: 'app-bardetails',
  templateUrl: './bardetails.page.html',
  styleUrls: ['./bardetails.page.scss'],
})
export class BardetailsPage implements OnInit {
  @ViewChild('map',{ static: true }) mapContainer: ElementRef;
  map: any;
  data = '';
  bar_id = '';
  url = 'https://daru.club/api/bardetails';
  results: any;
  reviews: any;
  sliderImages: any = [];

  constructor(private launchNavigator: LaunchNavigator,private router: Router, private route: ActivatedRoute, private http: HttpClient, private storage: Storage, private toastController: ToastController,private geolocation: Geolocation) {
    // this.bar_id = this.route.snapshot.paramMap.get("bar_id")
    this.route.queryParams.subscribe(params => {
      this.bar_id = params.bar_id;
      this.getBarDetails();
    })

    

    // this.route.paramMap.subscribe(params => {
    //     // this.bar_id =  params['bar_id'];
    //     console.log(params);
    //     this.getBarDetails();
    // });
  }

  ngOnInit() {
    



  }
  openmap(address){
    let options: LaunchNavigatorOptions = {
    // start: address,
  
    }
    this.launchNavigator.navigate(address, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
  getBarDetails() {
    this.http.get(`${this.url}` + '?bar_id=' + this.bar_id, this.results).subscribe((res) => {
      this.results = res['bar_details'];
      this.reviews = res['reviews'];
      this.sliderImages = res['images']
      
    });
    this.displayGoogleMap(this.results);
  }

  displayGoogleMap(results) {
    const latLng = new google.maps.LatLng(28.4222, 77.4232);
    const mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapContainer.nativeElement, mapOptions);
    const marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng
    });
    this.addInfoWindow(marker, "delhi");
  }
  addInfoWindow(marker, content) {
    const infoWindow = new google.maps.InfoWindow({
      content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  bookNow(barId: any) {
    this.router.navigate(['/bookingpage'], { queryParams: { bar_id: barId } });

  }

}
