import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, RouterModule, Routes } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-selectfilter',
  templateUrl: './selectfilter.page.html',
  styleUrls: ['./selectfilter.page.scss'],
})
export class SelectfilterPage implements OnInit {
	distance: string;
	time: string;
	drink_type: string;
	url = 'https://daru.club/api/register';
	results: any ;

  constructor(private route: ActivatedRoute, public httpClient: HttpClient, public router: Router) { }

  ngOnInit() {
  }

  searchpost() {
  	this.drink_type = this.route.snapshot.paramMap.get("drink_type")

    let poster = new FormData();
  	// console.log(this.id);
  	poster.append('distance', this.distance);
  	poster.append('time', this.time);
  	

  	this.httpClient.post(`${this.url}`,poster, this.results).subscribe(res=>{

  		this.router.navigate(['brandlist', { time: this.time, distance: this.distance, drink_type: this.drink_type}]);
		} , error => {
        console.log(error);
      });
  }

}
