import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectfilterPage } from './selectfilter.page';

describe('SelectfilterPage', () => {
  let component: SelectfilterPage;
  let fixture: ComponentFixture<SelectfilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectfilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectfilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
