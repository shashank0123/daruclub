import { Component } from '@angular/core';
import { AuthService } from './../services/auth.service';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import Swiper from 'swiper';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  Swiper: any; 
	data = '';
  	url = 'https://daru.club/api/drink_type';
	results: any ;

  constructor(private http: HttpClient,   private authService: AuthService, private storage: Storage, private toastController: ToastController) {}

  ngOnInit() {
  	this.http.get(`${this.url}`, this.results).subscribe((res)=>{
			this.results = res['drink_type'];
		});

     var swiper = new Swiper('.swiper-container', {
      spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

  }
  loadSpecialInfo() {
    this.authService.getSpecialData().subscribe(res => {
      this.data = res['msg'];
      console.log(res);
    });
  }



  logout() {
    this.authService.logout();
  }

  clearToken() {
    // ONLY FOR TESTING!
    this.storage.remove('access_token');
 
    let toast = this.toastController.create({
      message: 'JWT removed',
      duration: 3000
    });
    toast.then(toast => toast.present());
  }



  
}
