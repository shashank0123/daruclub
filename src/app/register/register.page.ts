import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, RouterModule, Routes } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  url = 'https://daru.club/api/register';
  mobile: string;
  age: string;
  results: any;

  constructor(public httpClient: HttpClient, public router: Router, public alertController: AlertController) { }

  ngOnInit() {
  }

  sendPostRequest() {

    if (this.mobile ) {
      let poster = new FormData();
      if (this.age) {
        console.log(this.age)
          } else {
          this.presentAlert('Please accept the age');
        }
      // console.log(this.id);
      // console.log(this.id);

      poster.append('mobile', this.mobile);
      poster.append('age', this.age);


      this.httpClient.post(`${this.url}`, poster, this.results).subscribe(res => {
        console.log(res); if (res['message'] == 'Success. OTP Sent') {
          console.log(res['message'])

          this.router.navigate(['otp/' + this.mobile]);
        }
      }, error => {
        console.log(error);
      });
    } else {
      this.presentAlert('Please enter valid mobile');
    }
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: '',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

}
