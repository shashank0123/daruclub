import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import  { Router   } from '@angular/router';

@Component({
  selector: 'app-barlist',
  templateUrl: './barlist.page.html',
  styleUrls: ['./barlist.page.scss'],
})
export class BarlistPage implements OnInit {
	data = '';
  	url = 'https://daru.club/api/barlist';
	results: any ;

  constructor(private http: HttpClient, private storage: Storage, 
    private toastController: ToastController , private router: Router ) { }

  ngOnInit() {
  	this.http.get(`${this.url}`, this.results).subscribe((res)=>{
			this.results = res['bars'];
		});
  }


 barDetails( barId: any ){
     this.router.navigate(['/bardetails'], { queryParams: { bar_id: barId } });

 }

}
