import { AuthGuardService } from './services/auth-guard.service';
import { NgModule } from '@angular/core';
import Swiper from 'swiper';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [AuthGuardService]
  },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'otp/:mobile', loadChildren: './otp/otp.module#OtpPageModule' },
  { path: 'homepage', loadChildren: './homepage/homepage.module#HomepagePageModule', canActivate: [AuthGuardService] },
  { path: 'bookingpage', loadChildren: './bookingpage/bookingpage.module#BookingpagePageModule', canActivate: [AuthGuardService] },
  { path: 'mapview', loadChildren: './mapview/mapview.module#MapviewPageModule', canActivate: [AuthGuardService] },
  { path: 'mybookings', loadChildren: './mybookings/mybookings.module#MybookingsPageModule', canActivate: [AuthGuardService] },
  { path: 'searchpage', loadChildren: './searchpage/searchpage.module#SearchpagePageModule' },
  { path: 'myprofile', loadChildren: './myprofile/myprofile.module#MyprofilePageModule', canActivate: [AuthGuardService] },
  { path: 'forgotpassword', loadChildren: './forgotpassword/forgotpassword.module#ForgotpasswordPageModule' },
  { path: 'faq', loadChildren: './faq/faq.module#FaqPageModule' },
  { path: 'invoice', loadChildren: './invoice/invoice.module#InvoicePageModule', canActivate: [AuthGuardService] },
  { path: 'selectfilter/:drink_type', loadChildren: './selectfilter/selectfilter.module#SelectfilterPageModule' , canActivate: [AuthGuardService]},
  { path: 'brandlist/:type', loadChildren: './brandlist/brandlist.module#BrandlistPageModule', canActivate: [AuthGuardService] },
  { path: 'barlist/:brand', loadChildren: './barlist/barlist.module#BarlistPageModule', canActivate: [AuthGuardService] },
  { path: 'bardetails', loadChildren: './bardetails/bardetails.module#BardetailsPageModule', canActivate: [AuthGuardService] },
  { path: 'my-booking', loadChildren: './my-booking/my-booking.module#MyBookingPageModule' },
  { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  { path: 'entry-type/:type', loadChildren: './entry-type/entry-type.module#EntryTypePageModule' }





];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
