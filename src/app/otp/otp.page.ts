import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {
  url = 'https://daru.club/api/register';
  mobile;
  otp: string
  credentialsForm: FormGroup;
  results: any;

  timeLeft: number = 35;
  interval;

  constructor(
    private alertController: AlertController,
    public httpClient: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authService: AuthService) { }

  ngOnInit() {
    this.mobile = this.route.snapshot.paramMap.get("mobile")
    this.startTimer();
  }

  onSubmit() {
    let fd = new FormData();
    // console.log(this.id);
    fd.append('mobile', this.mobile);
    fd.append('otp', this.otp);

    if (this.otp) {
      this.pauseTimer();
      this.authService.login(fd).subscribe();
    }

  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        // this.timeLeft = 35;
        // this.router.navigate(['/registers']);
      }
    }, 1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

  resendPostRequest() {


    let poster = new FormData();
    // console.log(this.id);
    poster.append('mobile', this.mobile);
    this.httpClient.post(`${this.url}`, poster, this.results).subscribe(res => {
      // console.log(res);
      if (res['message'] == 'Success. OTP Sent') {
        // console.log(res['message'])
        this.pauseTimer();
        this.timeLeft = 35;
        this.startTimer();
        // this.router.navigate(['otp/' + this.mobile]);
      }
    }, error => {
      console.log(error);
    });

  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: '',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

}
