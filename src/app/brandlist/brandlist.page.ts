import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

//import { AuthService } from './../services/auth.service';

@Component({
  selector: 'app-brandlist',
  templateUrl: './brandlist.page.html',
  styleUrls: ['./brandlist.page.scss'],
})
export class BrandlistPage implements OnInit {

  data = this.route.snapshot.paramMap.get("type")
  url = 'https://daru.club/api/brandlist?type='+this.data;
	results: any ;

  constructor(private http: HttpClient, private storage: Storage, private route: ActivatedRoute, private toastController: ToastController) {}

  ngOnInit() {
  	this.http.get(`${this.url}`, this.results).subscribe((res)=>{
			this.results = res['brands'];
		});

  }


  
}
