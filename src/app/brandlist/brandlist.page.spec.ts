import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandlistPage } from './brandlist.page';

describe('BrandlistPage', () => {
  let component: BrandlistPage;
  let fixture: ComponentFixture<BrandlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
